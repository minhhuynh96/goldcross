## Features

- Equip with React, ES6 & Babel 6
- Build with Webpack
- Support [hot module replacement](https://webpack.github.io/docs/hot-module-replacement.html)
- Auto Open a new browser tab when Webpack loads, and reload the page when you modified the code

## How to use

First, you should clone the repo and install the dependencies.

```bash
$ git clone https://bitbucket.org/jiamingzhang/39-ulil-website-apps.git <yourAppName>
$ cd <yourAppName>
$ git checkout origin/react
$ npm install
```

Then, launch the boilerplate app.

```bash
$ npm start
```

You should see a new browser tap opening your app in http://127.0.0.1:{PORT}.

From there, you start to develop your own code in the `app` directory. When you finish coding, use `npm run test` or `npm run deploy` to build the static files.

## How to deploy to ZCODER server

1. Use `npm run test` 
1. Commit code to `react` branch
1. Connect to ZCODER server and go to path `/home/ubuntu/project/39project`
1. use `nano .git/config` to change your account
1. use `git pull`

## How to deploy to PROD server (DRAFT....)

1. Use `npm run deploy` 
1. Commit code to `react` branch
1. Create a pull request from bitbutket to `react` to `production` branch
1. Merged commit at `production` branch
1. Connect to PROD server and go to path `/home/zcoder/travelagent`
1. use `nano .git/config` to change your account
1. use `git pull`
