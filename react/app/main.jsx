import React from 'react';
import ReactDOM from 'react-dom';

import { Router, Route, browserHistory, IndexRedirect, IndexRoute } from 'react-router';

import Home from 'base/components/home/home';

import './main.scss';
import 'base/assets/plugins/bootstrap/css/bootstrap.min.css';
import 'base/assets/css/one.style.css';

import 'base/assets/css/footers/footer-v7.css';

import 'base/assets/plugins/animate.css';
import 'base/assets/plugins/line-icons/line-icons.css';
import 'base/assets/plugins/font-awesome/css/font-awesome.min.css';
import 'base/assets/plugins/pace/pace-flash.css';
import 'base/assets/plugins/owl-carousel/owl.carousel.css';
import 'base/assets/plugins/cube-portfolio/cubeportfolio/css/cubeportfolio.min.css';
// import 'base/assets/plugins/cube-portfolio/cubeportfolio/custom/custom-cubeportfolio.html';
import 'base/assets/plugins/gallery-2.21.3/css/blueimp-gallery.min.css';

import 'base/assets/css/custom.css';

import 'script-loader!base/assets/plugins/jquery/jquery.min.js';
import 'script-loader!base/assets/plugins/jquery/jquery-migrate.min.js';
import 'base/assets/plugins/cube-portfolio/cubeportfolio/js/jquery.cubeportfolio.min.js';
import 'script-loader!base/assets/plugins/bootstrap/js/bootstrap.min.js';
import 'script-loader!base/assets/plugins/smoothScroll.js';
import 'script-loader!base/assets/plugins/jquery.easing.min.js';
import 'script-loader!base/assets/plugins/pace/pace.min.js';
import 'script-loader!base/assets/plugins/jquery.parallax.js';
import 'script-loader!base/assets/plugins/counter/jquery.counterup.min.js';
import 'script-loader!base/assets/plugins/owl-carousel/owl.carousel.js';
import 'script-loader!base/assets/plugins/sky-forms-pro/skyforms/js/jquery.form.min.js';
import 'script-loader!base/assets/plugins/sky-forms-pro/skyforms/js/jquery.validate.min.js';
// import 'script-loader!base/assets/js/plugins/cube-portfolio/cube-portfolio-lightbox.js';
import 'script-loader!base/assets/js/one.app.js';

import './ie';
import cookie from 'react-cookie';

class App extends React.Component {
  constructor(props){
    super(props);
  }
  componentDidMount(){
    $("html, body").scrollTop();
  }
  render() {
    return (
      <div>
        {this.props.children}
      </div>
    );
  }
}

browserHistory.listen(function(ev){
  $("html, body").animate({scrollTop: 0}, 300)
})

function checkLogin(nextState, replace) {
  if(!UserBase.getToken() || !UserBase.getCurrent()){
    UserBase.logout();
  }else{
    //window.location.href = "/login";
  }
}

ReactDOM.render(
  <Router history={browserHistory}>
    <Route path="/" component={App}>
      <IndexRoute component={Home} />
    </Route>
  </Router>,
  document.body.appendChild(document.createElement('div'))
);
