import $ from 'jquery';


var Curl = {
  get(path,params, cb, cb_error) {
    $.ajax({
      method: 'GET',
      url: API_URL + path,
      data: params,
      headers: {
        'Accept': 'application/json',
        'access-token': params['access-token']
      },
      error: function(err){
        if(typeof cb_error == "function"){
          if(err.statusText == "error"){
            err.statusText = "500 Internal Server Error"
          }
          cb_error(err)
        }
      },
      success: function(res){
        if(typeof cb_error == "function"){
          if(res.code == 401) {
            console.warn(401)
          }
          if(res.code == 200){
            cb(res);
          }else{
            cb_error(res)
          }
        }
      }
    })
  },
  post(path,params, cb, cb_error, required) {
    $.ajax({
      method: 'POST',
      url: API_URL + path,
      data: params,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded',
        'access-token': params['access-token']
      },
      error: function(err){
        if(typeof cb_error == "function"){
          if(err.statusText == "error"){
            err.statusText = "500 Internal Server Error"
          }

          if(err.responseJSON){
            err.statusText = err.responseJSON.message || err.statusText;
          }
          cb_error(err)
        }
      },
      success: function(res){
        if(res.code == 200){
          cb(res);
        }else{
          cb_error(res)
        }
      }
    })
  }

  // uploadFile(path, file, cb, cb_error) {
  //   $.ajax({
  //     method: 'POST',
  //     url: API_URL + path +'?access-token='+User.getToken(),
  //     data: file,
  //     processData: false,
  //     contentType: false,
  //     headers: {
  //       'Accept': 'application/json'
  //     },
  //     error: function(err){
  //       if(typeof cb_error == "function"){
  //         if(err.statusText == "error"){
  //           err.statusText = "500 Internal Server Error"
  //         }
  //         cb_error(err)
  //       }
  //     },
  //     success: function(res){
  //       if(typeof cb_error == "function"){
  //         if(res.code == 401) {
  //           swal('Oops!', res.message, 'warning');
  //         }
  //         cb(res);
  //       }
  //     }
  //   })
  // }

};

export default Curl;
