import React from 'react';
import { Link, browserHistory } from 'react-router';


import Curl from '../base/curl';

export default class Home extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      sliders: []
    }

  }


  componentDidMount(){
    App.init();
    App.initCounter();
    App.initParallaxBg();

    var gridContainer = $('#grid-container');
    var gridContainer2 = $('#grid-container2');

    gridContainer.cubeportfolio({
        layoutMode: 'grid',
        rewindNav: true,
        scrollByPage: false,
        mediaQueries: [{
            width: 1100,
            cols: 6
        }, {
            width: 800,
            cols: 4
        }, {
            width: 500,
            cols: 3
        }, {
            width: 320,
            cols: 2
        }],
        defaultFilter: '*',
        animationType: 'rotateSides',
        gapHorizontal: 10,
        gapVertical: 10,
        gridAdjustment: 'responsive',
        caption: 'fadeIn',
        displayType: 'sequentially',
        displayTypeSpeed: 100,

        // lightbox
        lightboxDelegate: '.cbp-lightbox',
        lightboxGallery: true,
        lightboxTitleSrc: 'data-title',
        lightboxCounter: '<div class="cbp-popup-lightbox-counter">{{current}} of {{total}}</div>',

        // singlePage popup
        singlePageDelegate: '.cbp-singlePage',
        singlePageDeeplinking: true,
        singlePageStickyNavigation: true,
        singlePageCounter: '<div class="cbp-popup-singlePage-counter">{{current}} of {{total}}</div>',
        singlePageCallback: function(url, element) {
            // to update singlePage content use the following method: this.updateSinglePage(yourContent)
        },

        // singlePageInline
        singlePageInlineDelegate: '.cbp-singlePageInline',
        singlePageInlinePosition: 'below',
        singlePageInlineInFocus: true,
        singlePageInlineCallback: function(url, element) {
            // to update singlePageInline content use the following method: this.updateSinglePageInline(yourContent)
            var t = this;

            $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: 'html',
                    timeout: 5000
                })
                .done(function(result) {

                    t.updateSinglePageInline(result);

                })
                .fail(function() {
                    t.updateSinglePageInline("Đã xảy ra lỗi khi tải nội dung!");
                });
        }
    });

    gridContainer2.cubeportfolio({
        layoutMode: 'grid',
        rewindNav: true,
        scrollByPage: false,
        mediaQueries: [{
            width: 1100,
            cols: 5
        }, {
            width: 800,
            cols: 4
        }, {
            width: 500,
            cols: 3
        }, {
            width: 320,
            cols: 2
        }],
        defaultFilter: '*',
        animationType: 'rotateSides',
        gapHorizontal: 10,
        gapVertical: 10,
        gridAdjustment: 'responsive',
        caption: 'fadeIn',
        displayType: 'sequentially',
        displayTypeSpeed: 100,

        // lightbox
        lightboxDelegate: '.cbp-lightbox',
        lightboxGallery: true,
        lightboxTitleSrc: 'data-title',
        lightboxCounter: '<div class="cbp-popup-lightbox-counter">{{current}} of {{total}}</div>',

        // singlePage popup
        singlePageDelegate: '.cbp-singlePage',
        singlePageDeeplinking: true,
        singlePageStickyNavigation: true,
        singlePageCounter: '<div class="cbp-popup-singlePage-counter">{{current}} of {{total}}</div>',
        singlePageCallback: function(url, element) {
            // to update singlePage content use the following method: this.updateSinglePage(yourContent)
        },

        // singlePageInline
        singlePageInlineDelegate: '.cbp-singlePageInline',
        singlePageInlinePosition: 'below',
        singlePageInlineInFocus: true,
        singlePageInlineCallback: function(url, element) {
            // to update singlePageInline content use the following method: this.updateSinglePageInline(yourContent)
            var t = this;

            $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: 'html',
                    timeout: 5000
                })
                .done(function(result) {

                    t.updateSinglePageInline(result);

                })
                .fail(function() {
                    t.updateSinglePageInline("Đã xảy ra lỗi khi tải nội dung!");
                });
        }
    });
  }


  componentWillUnmount(){

  }

  render() {
    return (
      <div>
        {/* Intro Section */}
        <section id="intro" className="intro-section">
          <div className="container">
            <img className="logo hidden-xs" src="assets/img/logo.png" href="#" />
            <div className="site-name">
              <p className="first-line">DẦU THẢO DƯỢC Y HỌC CỔ TRUYỀN THÁI LAN</p>
              <p className="second-line">Thập Tự Vàng</p>
              <p className="third-line">Trị đau nhức - Kháng viêm - Tan bầm tím</p>
            </div>
            <div className="site-info" style={{fontSize: 14}}>
              <div>
                <p className="first-line">Giao dầu miễn phí tận nơi trên toàn quốc</p>
                <p className="second-line">
                  Thanh toán khi nhận dầu<br />
                  Cam kết chính hãng Thái Lan 100%
                </p>
                <hr style={{margin: '10px 0 10px 65%', borderTopColor: '#777'}} />
              </div>
              <p style={{position: 'absolute', right: 0, bottom: 10, width: 325, color: '#a10f2b'}}>
                Hotline chăm sóc khách hàng 24/7: <strong style={{fontSize: 16}}>0905 760 580</strong>
              </p>
            </div>
            {/* <div class="ship-logo">
        <p>Cam kết 100% chính hãng</p>
        <img src="assets/img/logistics-delivery-truck-in-movement.png" />
        <p>Miễn phí</p>
      </div> */}
          </div>
        </section>
        {/* End Intro Section */}
        {/*=== Header ===*/}
        <nav className="one-page-header navbar navbar-default navbar-fixed-top not-top" role="navigation">
          <div className="container">
            <div className="menu-container page-scroll">
              <button type="button" className="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span className="sr-only">Toggle navigation</span>
                <span className="icon-bar" />
                <span className="icon-bar" />
                <span className="icon-bar" />
              </button>
              <a className="navbar-brand" href="#cong-dung">
                {/*<div class="first-line"><span>G</span>old <span>C</span>ross</div>
            <div class="second-line"><span>Y</span>ellow <span>O</span>ld</div>*/}
                {/* <img class="logo" src="assets/img/logo.png" href="#"> */}
                <div className="second-line"><span>D</span>ầu <span>T</span>hập <span>T</span>ự <span>V</span>àng</div>
              </a>
            </div>
            {/* Collect the nav links, forms, and other content for toggling */}
            <div className="collapse navbar-collapse navbar-ex1-collapse">
              <div className="menu-container">
                <ul className="nav navbar-nav">
                  {/* <li class="page-scroll home">
              <a href="#body">TRANG CHỦ</a>
            </li> */}
                  <li className="page-scroll">
                    <a href="#cong-dung">CÔNG DỤNG</a>
                  </li>
                  <li className="page-scroll">
                    <a href="#san-pham">SẢN PHẨM</a>
                  </li>
                  <li className="page-scroll">
                    <a href="#giai-thuong-cube">GIẢI THƯỞNG</a>
                  </li>
                  {/*<li class="page-scroll">
              <a href="#danh-gia">ĐÁNH GIÁ</a>
            </li>*/}
                  <li className="page-scroll">
                    <a href="#lien-he">LIÊN HỆ</a>
                  </li>
                  <li className="page-scroll dat-hang">
                    <a href="#dat-hang">ĐẶT HÀNG</a>
                  </li>
                </ul>
              </div>
            </div>
            {/* /.navbar-collapse */}
          </div>
          {/* /.container */}
        </nav>
        {/*=== End Header ===*/}
        {/* ADS Banner */}
        {/* <section id="ads" class="ads-section">
    <ul class="ads-slider">
      <li class="item"><a href="#"><img src="assets/img/carousel/1.jpg" alt=""></a></li>
      <li class="item"><a href="#"><img src="assets/img/carousel/2.png" alt=""></a></li>
      <li class="item"><a href="#"><img src="assets/img/carousel/3.png" alt=""></a></li>
      <li class="item"><a href="#"><img src="assets/img/carousel/4.png" alt=""></a></li>
      <li class="item"><a href="#"><img src="assets/img/carousel/5.png" alt=""></a></li>
      <li class="item"><a href="#"><img src="assets/img/carousel/6.png" alt=""></a></li>
    </ul>
  </section> */}
        {/* End ADS Banner */}
        {/* Utility Section */}
        <section id="cong-dung" className="about-section">
          <div className="container content-lg" style={{paddingTop: 50}}>
            <div className="title-v1">
              <h2>Công Dụng &amp; CÁCH ĐIỀU TRỊ</h2>
              <div className="use-wrapper">
                <p>Dầu THẬP TỰ VÀNG được tổng hợp từ 7 loại thảo dược quý của Thái Lan</p>
                <p>Đã qua kiểm định <strong>100% ThẢO DƯỢC</strong> an toàn cho sức khỏe: 3 không</p>
                <p className="slogan">Không pha cồn - Không chứa các chất giảm - Không chứa các chất gây nghiện.</p>
              </div>
            </div>
            <div className="cube-portfolio">
              <div id="grid-container" className="cbp-l-grid-gallery">
                <div className="cbp-item print motion">
                  <a href="assets/ajax/congdung1.html" className="cbp-caption cbp-singlePageInline" data-title="World Clock Widget<br>by Paul Flavius Nechita">
                    <div className="cbp-caption-defaultWrap">
                      <img src="assets/img/cong-dung/1.jpg" alt />
                    </div>
                    <div className="cbp-caption-activeWrap">
                      <span>Chi tiết</span>
                    </div>
                  </a>
                </div>
                <div className="cbp-item web-design">
                  <a href="assets/ajax/congdung2.html" className="cbp-caption cbp-singlePageInline" data-title="Bolt UI<br>by Tiberiu Neamu">
                    <div className="cbp-caption-defaultWrap">
                      <img src="assets/img/cong-dung/2.jpg" alt />
                    </div>
                    <div className="cbp-caption-activeWrap">
                      <span>Chi tiết</span>
                    </div>
                  </a>
                </div>
                <div className="cbp-item print motion">
                  <a href="assets/ajax/congdung3.html" className="cbp-caption cbp-singlePageInline" data-title="WhereTO App<br>by Tiberiu Neamu">
                    <div className="cbp-caption-defaultWrap">
                      <img src="assets/img/cong-dung/3.jpg" alt />
                    </div>
                    <div className="cbp-caption-activeWrap">
                      <span>Chi tiết</span>
                    </div>
                  </a>
                </div>
                <div className="cbp-item web-design print">
                  <a href="assets/ajax/congdung4.html" className="cbp-caption cbp-singlePageInline" data-title="iDevices<br>by Tiberiu Neamu">
                    <div className="cbp-caption-defaultWrap">
                      <img src="assets/img/cong-dung/4.jpg" alt />
                    </div>
                    <div className="cbp-caption-activeWrap">
                      <span>Chi tiết</span>
                    </div>
                  </a>
                </div>
                <div className="cbp-item motion">
                  <a href="assets/ajax/congdung5.html" className="cbp-caption cbp-singlePageInline" data-title="Seemple* Music for iPad<br>by Tiberiu Neamu">
                    <div className="cbp-caption-defaultWrap">
                      <img src="assets/img/cong-dung/5.jpg" alt />
                    </div>
                    <div className="cbp-caption-activeWrap">
                      <span>Chi tiết</span>
                    </div>
                  </a>
                </div>
                <div className="cbp-item print motion">
                  <a href="assets/ajax/congdung6.html" className="cbp-caption cbp-singlePageInline" data-title="Remind~Me Widget<br>by Tiberiu Neamu">
                    <div className="cbp-caption-defaultWrap">
                      <img src="assets/img/cong-dung/6.jpg" alt />
                    </div>
                    <div className="cbp-caption-activeWrap">
                      <span>Chi tiết</span>
                    </div>
                  </a>
                </div>
                <div className="cbp-item web-design print">
                  <a href="assets/ajax/congdung7.html" className="cbp-caption cbp-singlePageInline" data-title="Workout Buddy<br>by Tiberiu Neamu">
                    <div className="cbp-caption-defaultWrap">
                      <img src="assets/img/cong-dung/7.jpg" alt />
                    </div>
                    <div className="cbp-caption-activeWrap">
                      <span>Chi tiết</span>
                    </div>
                  </a>
                </div>
                <div className="cbp-item print">
                  <a href="assets/ajax/congdung8.html" className="cbp-caption cbp-singlePageInline" data-title="Digital Menu<br>by Cosmin Capitanu">
                    <div className="cbp-caption-defaultWrap">
                      <img src="assets/img/cong-dung/8.jpg" alt />
                    </div>
                    <div className="cbp-caption-activeWrap">
                      <span>Chi tiết</span>
                    </div>
                  </a>
                </div>
                <div className="cbp-item motion">
                  <a href="assets/ajax/congdung9.html" className="cbp-caption cbp-singlePageInline" data-title="Holiday Selector<br>by Cosmin Capitanu">
                    <div className="cbp-caption-defaultWrap">
                      <img src="assets/img/cong-dung/9.jpg" alt />
                    </div>
                    <div className="cbp-caption-activeWrap">
                      <span>Chi tiết</span>
                    </div>
                  </a>
                </div>
                <div className="cbp-item print motion">
                  <a href="assets/ajax/congdung10.html" className="cbp-caption cbp-singlePageInline" data-title="World Clock Widget<br>by Paul Flavius Nechita">
                    <div className="cbp-caption-defaultWrap">
                      <img src="assets/img/cong-dung/10.jpg" alt />
                    </div>
                    <div className="cbp-caption-activeWrap">
                      <span>Chi tiết</span>
                    </div>
                  </a>
                </div>
                <div className="cbp-item web-design">
                  <a href="assets/ajax/congdung11.html" className="cbp-caption cbp-singlePageInline" data-title="Bolt UI<br>by Tiberiu Neamu">
                    <div className="cbp-caption-defaultWrap">
                      <img src="assets/img/cong-dung/11.jpg" alt />
                    </div>
                    <div className="cbp-caption-activeWrap">
                      <span>Chi tiết</span>
                    </div>
                  </a>
                </div>
                <div className="cbp-item print motion">
                  <a href="assets/ajax/congdung12.html" className="cbp-caption cbp-singlePageInline" data-title="WhereTO App<br>by Tiberiu Neamu">
                    <div className="cbp-caption-defaultWrap">
                      <img src="assets/img/cong-dung/12.jpg" alt />
                    </div>
                    <div className="cbp-caption-activeWrap">
                      <span>Chi tiết</span>
                    </div>
                  </a>
                </div>
                <div className="cbp-item web-design print">
                  <a href="assets/ajax/congdung13.html" className="cbp-caption cbp-singlePageInline" data-title="iDevices<br>by Tiberiu Neamu">
                    <div className="cbp-caption-defaultWrap">
                      <img src="assets/img/cong-dung/13.jpg" alt />
                    </div>
                    <div className="cbp-caption-activeWrap">
                      <span>Chi tiết</span>
                    </div>
                  </a>
                </div>
                <div className="cbp-item motion">
                  <a href="assets/ajax/congdung14.html" className="cbp-caption cbp-singlePageInline" data-title="Seemple* Music for iPad<br>by Tiberiu Neamu">
                    <div className="cbp-caption-defaultWrap">
                      <img src="assets/img/cong-dung/14.jpg" alt />
                    </div>
                    <div className="cbp-caption-activeWrap">
                      <span>Chi tiết</span>
                    </div>
                  </a>
                </div>
                <div className="cbp-item print motion">
                  <a href="assets/ajax/congdung15.html" className="cbp-caption cbp-singlePageInline" data-title="Remind~Me Widget<br>by Tiberiu Neamu">
                    <div className="cbp-caption-defaultWrap">
                      <img src="assets/img/cong-dung/15.jpg" alt />
                    </div>
                    <div className="cbp-caption-activeWrap">
                      <span>Chi tiết</span>
                    </div>
                  </a>
                </div>
                <div className="cbp-item web-design print">
                  <a href="assets/ajax/congdung16.html" className="cbp-caption cbp-singlePageInline" data-title="Workout Buddy<br>by Tiberiu Neamu">
                    <div className="cbp-caption-defaultWrap">
                      <img src="assets/img/cong-dung/16.jpg" alt />
                    </div>
                    <div className="cbp-caption-activeWrap">
                      <span>Chi tiết</span>
                    </div>
                  </a>
                </div>
                <div className="cbp-item print">
                  <a href="assets/ajax/congdung17.html" className="cbp-caption cbp-singlePageInline" data-title="Digital Menu<br>by Cosmin Capitanu">
                    <div className="cbp-caption-defaultWrap">
                      <img src="assets/img/cong-dung/17.jpg" alt />
                    </div>
                    <div className="cbp-caption-activeWrap">
                      <span>Chi tiết</span>
                    </div>
                  </a>
                </div>
                <div className="cbp-item motion">
                  <a href="assets/ajax/congdung18.html" className="cbp-caption cbp-singlePageInline" data-title="Holiday Selector<br>by Cosmin Capitanu">
                    <div className="cbp-caption-defaultWrap">
                      <img src="assets/img/cong-dung/18.jpg" alt />
                    </div>
                    <div className="cbp-caption-activeWrap">
                      <span>Chi tiết</span>
                    </div>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* End Utility Section */}
        {/* Products Section */}
        <section id="san-pham" className="news-section bg-grey">
          <div className="container content-lg">
            <div className="title-v1">
              <h2>SẢN PHẨM</h2>
              <p>Dầu thảo dược Thập tự vàng có 3 cỡ chai khác nhau để khách hàng lựa chọn.</p>
            </div>
            <div className="row news-v1">
              <div className="col-md-4 md-margin-bottom-40">
                <div className="news-v1-in">
                  <img className="img-responsive" src="assets/img/products/1.png" alt />
                  <h3><a href="#">Chai nhỏ</a></h3>
                  <p>Dung tính: 3 ml<br />Giá bán: 60.000 VNĐ</p>
                </div>
              </div>
              <div className="col-md-4 md-margin-bottom-40">
                <div className="news-v1-in">
                  <img className="img-responsive" src="assets/img/products/2.png" alt />
                  <h3><a href="#">Chai trung</a></h3>
                  <p>Dung tính: 24 ml<br />Giá bán: 180.000 VNĐ</p>
                </div>
              </div>
              <div className="col-md-4">
                <div className="news-v1-in">
                  <img className="img-responsive" src="assets/img/products/3.png" alt />
                  <h3><a href="#">Chai lớn ( có đầu xịt )</a></h3>
                  <p>Dung tính: 60 ml<br />Giá bán: 395.000 VNĐ</p>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* End Products Section */}
        {/* Giai thuong */}
        <section id="giai-thuong-cube" className="about-section">
          <div className="container content-lg">
            <div className="title-v1">
              <div className="title-v1">
                <h2>GIẢI THƯỞNG</h2>
              </div>
            </div>
            <div className="cube-portfolio">
              <div id="grid-container2" className="cbp-l-grid-gallery">
                <div className="cbp-item cbp-item-1 print motion">
                  <a href="assets/ajax/giaiThuong.html" className="cbp-caption cbp-singlePageInline" data-title="World Clock Widget<br>by Paul Flavius Nechita">
                    <div className="cbp-caption-defaultWrap">
                    </div>
                    <div className="cbp-caption-activeWrap">
                      <span>Chi tiết</span>
                    </div>
                  </a>
                </div>
                <div className="cbp-item cbp-item-2 print motion">
                  <a href="assets/ajax/giaiThuong.html" className="cbp-caption cbp-singlePageInline" data-title="World Clock Widget<br>by Paul Flavius Nechita">
                    <div className="cbp-caption-defaultWrap">
                    </div>
                    <div className="cbp-caption-activeWrap">
                      <span>Chi tiết</span>
                    </div>
                  </a>
                </div>
                <div className="cbp-item cbp-item-3 print motion">
                  <a href="assets/ajax/giaiThuong.html" className="cbp-caption cbp-singlePageInline" data-title="World Clock Widget<br>by Paul Flavius Nechita">
                    <div className="cbp-caption-defaultWrap">
                    </div>
                    <div className="cbp-caption-activeWrap">
                      <span>Chi tiết</span>
                    </div>
                  </a>
                </div>
                <div className="cbp-item cbp-item-4 print motion">
                  <a href="assets/ajax/giaiThuong.html" className="cbp-caption cbp-singlePageInline" data-title="World Clock Widget<br>by Paul Flavius Nechita">
                    <div className="cbp-caption-defaultWrap">
                    </div>
                    <div className="cbp-caption-activeWrap">
                      <span>Chi tiết</span>
                    </div>
                  </a>
                </div>
                <div className="cbp-item cbp-item-5 print motion">
                  <a href="assets/ajax/giaiThuong.html" className="cbp-caption cbp-singlePageInline" data-title="World Clock Widget<br>by Paul Flavius Nechita">
                    <div className="cbp-caption-defaultWrap">
                    </div>
                    <div className="cbp-caption-activeWrap">
                      <span>Chi tiết</span>
                    </div>
                  </a>
                </div>
                <div className="cbp-item cbp-item-6 print motion">
                  <a href="assets/ajax/giaiThuong.html" className="cbp-caption cbp-singlePageInline" data-title="World Clock Widget<br>by Paul Flavius Nechita">
                    <div className="cbp-caption-defaultWrap">
                    </div>
                    <div className="cbp-caption-activeWrap">
                      <span>Chi tiết</span>
                    </div>
                  </a>
                </div>
                <div className="cbp-item cbp-item-7 print motion">
                  <a href="assets/ajax/giaiThuong.html" className="cbp-caption cbp-singlePageInline" data-title="World Clock Widget<br>by Paul Flavius Nechita">
                    <div className="cbp-caption-defaultWrap">
                    </div>
                    <div className="cbp-caption-activeWrap">
                      <span>Chi tiết</span>
                    </div>
                  </a>
                </div>
                <div className="cbp-item cbp-item-8 print motion">
                  <a href="assets/ajax/giaiThuong.html" className="cbp-caption cbp-singlePageInline" data-title="World Clock Widget<br>by Paul Flavius Nechita">
                    <div className="cbp-caption-defaultWrap">
                    </div>
                    <div className="cbp-caption-activeWrap">
                      <span>Chi tiết</span>
                    </div>
                  </a>
                </div>
                <div className="cbp-item cbp-item-9 print motion">
                  <a href="assets/ajax/giaiThuong.html" className="cbp-caption cbp-singlePageInline" data-title="World Clock Widget<br>by Paul Flavius Nechita">
                    <div className="cbp-caption-defaultWrap">
                    </div>
                    <div className="cbp-caption-activeWrap">
                      <span>Chi tiết</span>
                    </div>
                  </a>
                </div>
                <div className="cbp-item cbp-item-10 print motion">
                  <a href="assets/ajax/giaiThuong.html" className="cbp-caption cbp-singlePageInline" data-title="World Clock Widget<br>by Paul Flavius Nechita">
                    <div className="cbp-caption-defaultWrap">
                    </div>
                    <div className="cbp-caption-activeWrap">
                      <span>Chi tiết</span>
                    </div>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* End Giai thuong */}
        {/*<div id="giai-thuong" class="clients-section parallaxBg">
        <div class="container">
            <div class="title-v1">
                <h2>GIẢI THƯỞNG</h2>
            </div>
            <ul class="owl-clients-v2">
                <li class="item item-1"><a href="assets/img/giai-thuong/1.jpg" title=""></a></li>
                <li class="item item-2"><a href="assets/img/giai-thuong/2.jpg" title=""></a></li>
                <li class="item item-3"><a href="assets/img/giai-thuong/3.jpg" title=""></a></li>
                <li class="item item-4"><a href="assets/img/giai-thuong/4.jpg" title=""></a></li>
                <li class="item item-5"><a href="assets/img/giai-thuong/5.jpg" title=""></a></li>
                <li class="item item-6"><a href="assets/img/giai-thuong/6.jpg" title=""></a></li>
                <li class="item item-7"><a href="assets/img/giai-thuong/7.jpg" title=""></a></li>
                <li class="item item-8"><a href="assets/img/giai-thuong/8.jpg" title=""></a></li>
                <li class="item item-9"><a href="assets/img/giai-thuong/9.jpg" title=""></a></li>
                <li class="item item-10"><a href="assets/img/giai-thuong/10.jpg" title=""></a></li>
            </ul>
        </div>
    </div>*/}
        {/*<section id="danh-gia" class="testimonials-v3">
    <div class="container content-lg">
      <div class="title-v1">
        <h2>Ý kiến khách hàng</h2>
        <p>We do <strong>things</strong> differently company providing key digital services. <br>
          Focused on helping our clients to build a <strong>successful</strong> business on web and mobile.</p>
      </div>

      <div class="row service-box-v1">
        <div class="col-md-4 col-sm-6">
          <div class="service-block service-block-default">
            <img class="rounded-x img-bordered" src="assets/img/team/img3-sm.jpg" alt="">
            <h2 class="heading-md">Web Design</h2>
            <p>Donec id elit non mi porta gravida at eget metus id elit mi egetine. Fusce dapibus</p>
          </div>
        </div>
        <div class="col-md-4 col-sm-6">
          <div class="service-block service-block-default">
            <img class="rounded-x img-bordered" src="assets/img/team/img2-sm.jpg" alt="">
            <h2 class="heading-sm">Marketing &amp; Cunsulting</h2>
            <p>Donec id elit non mi porta gravida at eget metus id elit mi egetine usce dapibus elit nondapibus</p>
          </div>
        </div>
        <div class="col-md-4 col-sm-12">
          <div class="service-block service-block-default">
            <img class="rounded-x img-bordered" src="assets/img/team/img1-sm.jpg" alt="">
            <h2 class="heading-sm">SEO &amp; Advertising</h2>
            <p>Donec id elit non mi porta gravida at eget metus id elit mi egetine. Fusce dapibus</p>
          </div>
        </div>
      </div>
    </div>
  </section>*/}
        {/* Booking form */}
        <section id="dat-hang" className="booking-form">
          <form action="dat-hang" method="POST" id="sky-form4" className="form-inline">
            <input type="hidden" name="_token" defaultValue="{!! csrf_token() !!}" />
            <span>Giao hàng tận nơi miễn phí toàn quốc. Đặt hàng ngay</span>
            <div className="form-group">
              <label className="sr-only" htmlFor="exampleInputEmail3">Tên</label>
              <input type="text" name="txtName" className="form-control" id="exampleInputEmail3" placeholder="Tên" />
            </div>
            <div className="form-group">
              <label className="sr-only" htmlFor="exampleInputPassword3">Số điện thoại</label>
              <input type="text" name="txtPhone" className="form-control" id="exampleInputPassword3" placeholder="Số điện thoại" />
            </div>
            <div className="form-group">
              <label className="sr-only" htmlFor="exampleInputNumber">Số lượng</label>
              <input type="text" name="txtNumber" className="form-control" id="exampleInputNumber" placeholder="Số lượng" />
            </div>
            <button type="submit" className="btn btn-default">Đặt hàng</button>
          </form>
        </section>
        {/* End Booking form */}
        {/* Contact Section */}
        <section id="lien-he" className="contacts-section">
          <div className="container content-lg">
            <div className="title-v1">
              <h2>HỖ TRỢ VÀ CHĂM SÓC KHÁCH HÀNG</h2>
              <p>Nếu cần hỗ trợ thêm thông tin xin liên hệ với chúng tôi qua các số điện thoại: 0905183986 - 0905760580 - 0905727283<br />Bạn có thể liên hệ với chúng tôi bằng cách điền đầy đủ các thông tin bên dưới.</p>
            </div>
            <div className="row contacts-in">
              <div className="col-md-6 md-margin-bottom-40">
                <ul className="list-unstyled">
                  <li><i className="fa fa-home" />K30 H18/17 Trần Phú, Quận Hải Châu, TP Đà Nẵng</li>
                  <li><i className="fa fa-phone" />Đà nẵng: 0905 183 986 - 0905 760 580</li>
                  <li><i className="fa fa-envelope" /> <a href="mailto:dauthaptuvang@yahoo.com">dauthaptuvang@yahoo.com</a></li>
                </ul>
              </div>
              <div className="col-md-6">
                <form id="sky-form3" className="sky-form contact-style">
                  <input type="hidden" name="_token" defaultValue="{!! csrf_token() !!}" />
                  <fieldset>
                    <label>Tên <span className="color-red">*</span></label>
                    <div className="row">
                      <div className="col-md-7 margin-bottom-20 col-md-offset-0">
                        <div>
                          <input type="text" name="txtName" id="name" className="form-control" />
                        </div>
                      </div>
                    </div>
                    <label>Số điện thoại <span className="color-red">*</span></label>
                    <div className="row">
                      <div className="col-md-7 margin-bottom-20 col-md-offset-0">
                        <div>
                          <input type="text" name="txtPhone" id="email" className="form-control" />
                        </div>
                      </div>
                    </div>
                    <label>Nội dung <span className="color-red">*</span></label>
                    <div className="row">
                      <div className="col-md-11 margin-bottom-20 col-md-offset-0">
                        <div>
                          <textarea rows={8} name="txtContent" id="message" className="form-control" defaultValue={""} />
                        </div>
                      </div>
                    </div>
                    <p><button type="submit" className="btn-u btn-brd btn-brd-hover btn-u-dark">Gửi</button></p>
                  </fieldset>
                  <div className="message">
                    <i className="rounded-x fa fa-check" />
                    <p>Tin nhắn của bạn đã được chuyển đi thành công! Chúng tôi sẽ phản hồi sớm nhất có thế.</p>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div className="copyright-section">
            <p>2017 © All Rights Reserved.</p>
            <ul className="social-icons">
              <li><a href="#" data-original-title="Facebook" className="social_facebook rounded-x" /></li>
              <li><a href="#" data-original-title="Twitter" className="social_twitter rounded-x" /></li>
              <li><a href="#" data-original-title="Goole Plus" className="social_googleplus rounded-x" /></li>
              <li><a href="#" data-original-title="Pinterest" className="social_pintrest rounded-x" /></li>
              <li><a href="#" data-original-title="Linkedin" className="social_linkedin rounded-x" /></li>
            </ul>
            <span className="page-scroll"><a href="#intro"><i className="fa fa-angle-double-up back-to-top" /></a></span>
          </div>
        </section>
        {/* End Contact Section */}
        <div id="blueimp-gallery" className="blueimp-gallery">
          <div className="slides" />
          <h3 className="title" />
          <a className="prev">‹</a>
          <a className="next">›</a>
          <a className="close">×</a>
          <a className="play-pause" />
          <ol className="indicator" />
        </div>

      </div>
    )
  }
}
