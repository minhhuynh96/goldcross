var ContactForm = function () {

    return {

        //Contact Form
        initContactForm: function () {
            // Validation
            $("#sky-form3").validate({
                // Rules for form validation
                rules:
                {
                    txtName:
                    {
                        required: true
                    },
                    txtPhone:
                    {
                        required: true,
                    },
                    txtContent:
                    {
                        required: true,
                        minlength: 10
                    },
                    captcha:
                    {
                        required: true,
                        remote: 'assets/plugins/sky-forms/version-2.0.1/captcha/process.php'
                    }
                },

                // Messages for form validation
                messages:
                {
                    txtName:
                    {
                        required: 'Vui lòng nhập tên của bạn',
                    },
                    txtPhone:
                    {
                        required: 'Vui lòng nhập số điện thoại của bạn',
                    },
                    txtContent:
                    {
                        required: 'Vui lòng nhập nội dung liên hệ'
                    },
                    captcha:
                    {
                        required: 'Please enter characters',
                        remote: 'Correct captcha is required'
                    }
                },

                // Ajax form submition
                submitHandler: function(form)
                {
                    $(form).ajaxSubmit(
                    {
                        beforeSend: function()
                        {
                            $('#sky-form3 button[type="submit"]').attr('disabled', true);
                        },
                        success: function()
                        {
                            $url = "lien-he";
                            $_token =  $('#sky-form3').find("input[name='_token']").val();
                            $txtName = $('#sky-form3').find("input[name='txtName']").val();
                            $txtPhone = $('#sky-form3').find("input[name='txtPhone']").val();
                            $txtContent = $('#sky-form3').find("textarea[name='txtContent']").val();
                            $data = {'_token': $_token};
                            $data["data"] = [$txtName, $txtPhone, $txtContent];
                            $.ajax({
                                url: $url,
                                type: 'POST',
                                cache: false,
                                data: $data,
                                success: function (data){
                                    if(data == "OK"){
                                        $("#sky-form3").addClass('submited');
                                    }
                                }
                            })

                        }
                    });
                },

                // Do not change code below
                errorPlacement: function(error, element)
                {
                    error.insertAfter(element.parent());
                }
            });
        }

    };

}();

function sendContact(){

    alert($_token);
}
