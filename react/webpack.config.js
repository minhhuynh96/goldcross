var webpack = require('webpack');
var path = require('path');
var OpenBrowserPlugin = require('open-browser-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var config = {
  host: 'http://127.0.0.1',
  port: 7790
};
module.exports = {
  devServer: {
    historyApiFallback: true,
    hot: true,
    inline: true,
    contentBase: './app',
    port: config.port
  },
  entry: {
    main: path.resolve(__dirname, 'app/main.jsx')
  },
  output: {
    filename: 'bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          { loader: "style-loader"},
          { loader: "css-loader" }
        ]
      },
      {
        test: /\.scss$/,
        use: [
          { loader: "style-loader"},
          { loader: "css-loader"},
          { loader: "sass-loader"}
        ]
      },
      {
        test: /\.js[x]?$/,
        use: [
          { loader: 'babel-loader' }
        ]
      },
      {
        test: /\.png$/,
        use: [
          { loader: "url-loader?limit=100000" }
        ]
      },
      {
        test: /\.jpg$/,
        use: [
          { loader: "file-loader" }
        ]
      },
      {
        test: /\.gif$/,
        use: [
          { loader: "file-loader" }
        ]
      },
      {
        test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          { loader: "file-loader" }
        ]
      },
      {
        test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          { loader:"url-loader?prefix=font/&limit=5000" }
        ]
      },
      {
        test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          { loader: "url-loader?limit=10000&mimetype=application/octet-stream" }
        ]
      },
      {
        test: /\.[ot]tf$/,
        use: [
          { loader: "url-loader?limit=10000&mimetype=application/octet-stream" }
        ]
      },
      {
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          { loader: "url-loader?limit=10000&mimetype=image/svg+xml" }
        ]
      }
    ]
  },
  resolve: {
    extensions: ['.js', '.jsx'],
    alias: {
      'base': path.resolve(__dirname, './app/'),
      'jquery': 'jquery/dist/jquery.js'
    }
  },
  plugins: [
    new webpack.ProvidePlugin({
      $: "jquery",
      jquery: "jquery",
      jQuery: "jquery",
      "window.jQuery": "jquery",
      "Tether": 'tether'
    }),
    new webpack.HotModuleReplacementPlugin(),
    new OpenBrowserPlugin({ url: config.host+':'+config.port }),
    new webpack.DefinePlugin({
        API_URL: JSON.stringify("http://api-dev.ulil.zcoder.io:4001"),
    }),
    new HtmlWebpackPlugin({
      template: 'app/index.html'
    })
  ]
};
