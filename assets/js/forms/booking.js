var BookingForm = function () {

    return {

        //Contact Form
        initBookingForm: function () {
            // Validation
            $("#sky-form4").validate({
                // Rules for form validation
                rules:
                {
                    txtName:
                    {
                        required: true
                    },
                    txtPhone:
                    {
                        required: true,
                    },
                    captcha:
                    {
                        required: true,
                        remote: 'assets/plugins/sky-forms/version-2.0.1/captcha/process.php'
                    }
                },

                // Messages for form validation
                messages:
                {
                    txtName:
                    {
                        required: 'Vui lòng nhập tên của bạn',
                    },
                    txtPhone:
                    {
                        required: 'Vui lòng nhập số điện thoại của bạn',
                    },
                    captcha:
                    {
                        required: 'Please enter characters',
                        remote: 'Correct captcha is required'
                    }
                },

                // Ajax form submition
                submitHandler: function(form)
                {
                    $(form).ajaxSubmit(
                    {
                        beforeSend: function()
                        {
                            $('#sky-form4 button[type="submit"]').attr('disabled', true);
                        },
                        success: function()
                        {
                            var url = "dat-hang";
                            var _token =  $('#sky-form4').find("input[name='_token']").val();
                            var txtName = $('#sky-form4').find("input[name='txtName']").val();
                            var txtPhone = $('#sky-form4').find("input[name='txtPhone']").val();
                            var qty_lon = $('#sky-form4').find("select[name='qty_lon']").val();
                            var qty_trungbinh = $('#sky-form4').find("select[name='qty_trungbinh']").val();
                            var qty_nho = $('#sky-form4').find("select[name='qty_nho']").val();
                            var data = {'_token': _token};
                            data["data"] = [txtName, txtPhone, qty_lon, qty_trungbinh, qty_nho];
                            $.ajax({
                                url: url,
                                type: 'POST',
                                cache: false,
                                data: data,
                                success: function (data){
                                    $('#sky-form4 button[type="submit"]').attr('disabled', false);
                                    switch(data){
                                        case "Error":
                                            alert("Lỗi! Vui lòng liên hệ Admin");
                                            break;
                                        case "Error1":
                                            alert("Vui lòng nhập tên và số điện thoại");
                                            break;
                                        case "Error2":
                                            alert("Vui lòng chọn số lượng sản phẩm");
                                            break;
                                        case "OK":
                                            alert("Đặt hàng thành công!");
                                            $('#sky-form4').find("input[name='txtName']").val("");
                                            $('#sky-form4').find("input[name='txtPhone']").val("");
                                            $('#sky-form4').find("select[name='qty_lon']").val(0);
                                            $('#sky-form4').find("select[name='qty_trungbinh']").val(0);
                                            $('#sky-form4').find("select[name='qty_nho']").val(0);
                                            break;
                                    }
                                }
                            })
                        }
                    });
                },

                // Do not change code below
                errorPlacement: function(error, element)
                {
                    error.insertAfter(element.parent());
                }
            });
        }

    };

}();

function sendContact(){

    alert($_token);
}
